package exception_exo;

public class IncorrectCodePostalException extends Exception{
	
	/**
	 * cl� de hashage
	 */
	private static final long serialVersionUID = 1L;

	public IncorrectCodePostalException(String cp) {
		System.out.println("Le code postal '" + cp + "' doit contenir exactement 5 chiffres");
	}	

}
