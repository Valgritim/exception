package exception_exo;

public class IncorrectStreetNameException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public IncorrectStreetNameException(String rue) {
		System.out.println("Toutes les lettres du nom de la rue '" + rue + "' doivent �tre en majuscule!");
	}	
}
