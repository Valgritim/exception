package exception_exo2;

public class AdresseException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public AdresseException(String cp, String rue) {
		
		if(!rue.equals(rue.toUpperCase())) {
			System.out.println("Toutes les lettres du nom de la rue '" + rue + "' doivent �tre en majuscule!");
		}
		if(cp.length() != 5) {
			System.out.println("Le code postal '" + cp + "' doit contenir exactement 5 chiffres");
		}	
		
		
	}

}
