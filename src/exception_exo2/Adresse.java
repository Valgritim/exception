package exception_exo2;

public class Adresse {
	
	private String rue;
	private String ville;
	private String codePostal;
	
	public Adresse(String rue, String ville, String codePostal) throws AdresseException {
		super();	
		
		if((codePostal.length() != 5) || (rue != rue.toUpperCase())) {			
			throw new AdresseException(codePostal,rue);
		}

		this.rue = rue;
		this.ville = ville;
		this.codePostal = codePostal;
		
	}
	public String getRue() {
		return rue;
	}
	public void setRue(String rue) throws AdresseException {

		this.rue = rue;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public String getCodePostal() {
		return codePostal;
	}
	public void setCodePostal(String codePostal) throws AdresseException {
		
		
		this.codePostal = codePostal;
		
	}
	
	@Override
	public String toString() {
		return "Adresse [rue=" + rue + ", ville=" + ville + ", codePostal=" + codePostal + "]";
	}
	
	

}
