package exception_exo3;

public class Saisie {
	
	private Integer entier;
	private boolean instanceOK;

	public Saisie(String entier) throws EntierException {
		super();
		
		if(entier <= 10) {			
			throw new EntierException("inf10");
		} else if (!(entier instanceof Integer)) {
			throw new EntierException("else");
		} else {
		
		this.entier = entier;
		System.out.println("Vous avez bien saisi un entier!");
		instanceOK = true;
		}
	}

	public Integer getEntier() {
		return entier;
	}

	public void setEntier(Integer entier) throws Exception {
		if(!(entier instanceof Integer)) {			
			throw new Exception();
		}
		this.entier = entier;
	}
	
	
	
	
	
	

}
